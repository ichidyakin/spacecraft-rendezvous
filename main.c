/*
 * Rendezvous of two spacecrafts
 */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>

const double Re = 6371.0;           // km, Earth radius
const double mju = 3.986028e5;      // km3/sec2, TBD
#define RAD_TO_DEG (180 / M_PI)
#define DEG_TO_RAD (M_PI / 180)

struct ORBIT_DATA {
    double H_min;                   // km, Minimal Height
    double H_max;                   // km, Maximal Height
    double U_prg;                   // deg, TBD
    double i;                       // deg, TBD
    double SIGMA;                   // deg, TBD
    double U;                       // deg, Angle position of spacecraft
    int N0;                      // -, Starting turn
    int N_ap;                    // -, Rendezvous turn
};

struct INPUT_DATA {
    struct ORBIT_DATA start;        // -, Starting orbit of active spacecraft
    struct ORBIT_DATA rendezvous;   // -, Starting orbit of passive spacecraft
    double precision;               // s, Required precision on delta T (1e-6 recommended)
    unsigned int max_iterations;            // -, Maximum iterations count to calculate (4 is recommended)
    double U_ap;                    // deg, Angle position of rendezvous point
};

struct MANEUVERS_DATA {
    //double delta_T;                 // s, Time error
    double a_H;
    double a_k;
    double delta_a_initial;
    double delta_aI;
    double delta_aII;
    double delta_aS;
    double delta_aS_initial;
    double delta_e;
    double delta_e_x;
    double delta_e_y;
    double delta_gamma_deg;
    double delta_gamma_rad;
    double delta_i_deg;
    double delta_i_rad;
    double delta_phi;
    double delta_SIGMA;
    double delta_t;
    double delta_t_initial;
    double delta_tr;
    double delta_Vt_I1;             // m/s
    double delta_Vt_I2;             // m/s
    double delta_Vt_II1;            // m/s
    double delta_Vt_II2;            // m/s
    double delta_VtI;
    double delta_VtII;
    double delta_Vz_I1;             // m/s
    double delta_Vz_I2;             // m/s
    double delta_Vz_II1;            // m/s
    double delta_Vz_II2;            // m/s
    double delta_VzI;
    double delta_VzII;
    double e_H;
    double e_k;
    double k1;
    double lambda;
    double next_delta_t;
    double phi_e;
    double phi_z;
    double phi1;
    double phi1_initial;
    double phi1z;
    double phi2;
    double phi2z;
    double phi3;
    double phi4;
    double phiS1;
    double r0;
    double T_H;
    double t_H;
    double T_k;
    double t_k;
    double time_error;
    double u_z;
    double V0;
};

struct MANEUVERS_DATA *calculate_maneuver(struct INPUT_DATA *input, struct MANEUVERS_DATA *man);

int resolve_inputs(int argc, char **pString, struct INPUT_DATA *pDATA);

int validate_inputs(struct INPUT_DATA *data);

void save_results(unsigned int iteraion, struct MANEUVERS_DATA *state);

void calculate_initial(struct INPUT_DATA *pDATA, struct MANEUVERS_DATA *pMANEUVERS_data);

double K(double phi);

void swap(double *a, double *b);

void print_solution(const struct MANEUVERS_DATA *state, unsigned int iteration);

void shift_braking_angle(double *a, double *phi);

int main(int argc, char *argv[]) {
    printf("Rendezvous of two spacecrafts\n");

    struct INPUT_DATA input;
    printf("Loading data...\n");
    if (resolve_inputs(argc, argv, &input) == 0) {
        printf("Validating data...\n");
        if (validate_inputs(&input) == 0) {
            printf("Calculating...\n");
            struct MANEUVERS_DATA *state = NULL;
            unsigned int iteration = 0;
            state = calculate_maneuver(&input, state);
            while (iteration++ < input.max_iterations && fabs(state->time_error) > input.precision) {
                print_solution(state, iteration);
                state = calculate_maneuver(&input, state);
            }
            print_solution(state, iteration);
            save_results(iteration, state);
        }
    }

    return 0;
}

void print_solution(const struct MANEUVERS_DATA *state, unsigned int iteration) {
    printf("\nIteration: %3d. [delta T] error = %f (%f sec)\n", iteration, state->time_error,
           state->time_error / state->lambda);
    printf("[delta t]: %3.7f. (%3.7f sec)\n", state->delta_t, state->delta_t / state->lambda);
    printf("Impulse 1: [dVt]: %3.5f; [dVz]: %3.5f; [phi]: %3.5f (%3.4f deg)\n",
           state->delta_Vt_I1 * state->V0 * 1000, state->delta_Vz_I1 * state->V0 * 1000, state->phi1,
           360 + fmod(state->phi1 * RAD_TO_DEG, 360));
    printf("Impulse 2: [dVt]: %3.5f; [dVz]: %3.5f; [phi]: %3.5f (%3.4f deg)\n",
           state->delta_Vt_I2 * state->V0 * 1000, state->delta_Vz_I2 * state->V0 * 1000, state->phi2,
           360 + fmod(state->phi2 * RAD_TO_DEG, 360));
    printf("Impulse 3: [dVt]: %3.5f; [dVz]: %3.5f; [phi]: %3.5f (%3.4f deg)\n",
           state->delta_Vt_II1 * state->V0 * 1000, state->delta_Vz_II1 * state->V0 * 1000, state->phi3,
           360 + fmod(state->phi3 * RAD_TO_DEG, 360));
    printf("Impulse 4: [dVt]: %3.5f; [dVz]: %3.5f; [phi]: %3.5f (%3.4f deg)\n",
           state->delta_Vt_II2 * state->V0 * 1000, state->delta_Vz_II2 * state->V0 * 1000, state->phi4,
           360 + fmod(state->phi4 * RAD_TO_DEG, 360));
}

void save_results(unsigned int iteraion, struct MANEUVERS_DATA *state) {
    return; // not implemented
}

int validate_inputs(struct INPUT_DATA *data) {
    // Converting degrees to radians
    data->start.U_prg *= DEG_TO_RAD;
    data->start.i *= DEG_TO_RAD;
    data->start.SIGMA *= DEG_TO_RAD;
    data->start.U *= DEG_TO_RAD;

    data->rendezvous.U_prg *= DEG_TO_RAD;
    data->rendezvous.i *= DEG_TO_RAD;
    data->rendezvous.SIGMA *= DEG_TO_RAD;
    data->rendezvous.U *= DEG_TO_RAD;

    // Converting km to m
    /*data->start.H_min *= 1000;
    data->start.H_max *= 1000;
    data->rendezvous.H_min *= 1000;
    data->rendezvous.H_max *= 1000;*/
    return 0;
}

int resolve_inputs(int argc, char **argv, struct INPUT_DATA *i) {
    if (argc < 2) {
        printf("Usage: rendezvous filename");
        exit(1);
    }
    char buf[100];
    printf("Processing \"%s\"\n", argv[1]);
    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL) {
        printf("Could not open file: error %d. Exiting...", errno);
        exit(1);
    }

    //fscanf(fp, "%s", buf); // # General parameters
    fgets(buf, sizeof(buf), fp);
    fscanf(fp, "%lf\n", &i->precision);
    fscanf(fp, "%d\n", &i->max_iterations);
    fscanf(fp, "%lf\n", &i->U_ap);
    //fgets(buf, sizeof(buf), fp); // # Start Orbit
    fgets(buf, sizeof(buf), fp); // # Start Orbit
    fscanf(fp, "%lf\n", &i->start.H_min);
    fscanf(fp, "%lf\n", &i->start.H_max);
    fscanf(fp, "%lf\n", &i->start.U_prg);
    fscanf(fp, "%lf\n", &i->start.i);
    fscanf(fp, "%lf\n", &i->start.SIGMA);
    fscanf(fp, "%lf\n", &i->start.U);
    fscanf(fp, "%d\n", &i->start.N0);
    fscanf(fp, "%d\n", &i->start.N_ap);
    fgets(buf, sizeof(buf), fp); // # Rendezvous Orbit
    fscanf(fp, "%lf\n", &i->rendezvous.H_min);
    fscanf(fp, "%lf\n", &i->rendezvous.H_max);
    fscanf(fp, "%lf\n", &i->rendezvous.U_prg);
    fscanf(fp, "%lf\n", &i->rendezvous.i);
    fscanf(fp, "%lf\n", &i->rendezvous.SIGMA);
    fscanf(fp, "%lf\n", &i->rendezvous.U);
    fscanf(fp, "%d\n", &i->rendezvous.N0);
    fscanf(fp, "%d", &i->rendezvous.N_ap);

    fclose(fp);

    return 0;
}

struct MANEUVERS_DATA *calculate_maneuver(struct INPUT_DATA *input, struct MANEUVERS_DATA *man) {
    if (man == NULL) {
        man = calloc(sizeof(struct MANEUVERS_DATA), 1);
        calculate_initial(input, man);
    }

    man->delta_t = man->next_delta_t;
    man->next_delta_t = 0;

    man->delta_VtI = man->delta_t / man->k1;
    man->delta_aI = 2 * man->delta_VtI;
    man->delta_aII = man->delta_a_initial - man->delta_aI;
    man->delta_aS = fabs(man->delta_aI) + fabs(man->delta_aII);

    man->phiS1 = 2 * atan((1 - man->delta_e / man->delta_aS) *
                          (-1 / tan(man->delta_phi) +
                           sqrt(1 / pow(tan(man->delta_phi), 2) + pow(man->delta_aS, 2)
                                                                  / (pow(man->delta_aS, 2) -
                                                                     pow(man->delta_e, 2)))));

    man->phi1 = man->phi_e - man->phiS1 + 2 * M_PI * (input->start.N0 - input->start.N_ap);
    man->phi3 = fmod(man->phi1, 2 * M_PI);

    man->delta_VtI = (pow(man->delta_e, 2) - pow(man->delta_aS, 2)) /
                     (4 * (man->delta_e_y * sin(man->phi1) + man->delta_e_x * cos(man->phi1) - man->delta_aS));
    man->delta_VtII = man->delta_aS / 2 - man->delta_VtI;

    man->phi2 = atan((man->delta_e_y / 2 - man->delta_VtI * sin(man->phi1))
                     / (man->delta_e_x / 2 - man->delta_VtI * cos(man->phi1)))
                + 2 * M_PI * (input->start.N0 - input->start.N_ap + 1);
    man->phi4 = fmod(man->phi2, 2 * M_PI);

    man->phi1z = man->phi1 - man->phi_z;
    man->phi2z = man->phi2 - man->phi_z;

    man->delta_VzI = -(man->delta_i_rad * sin(man->phi2z) / sin(man->phi1z - man->phi2z))
                     * man->delta_gamma_deg / man->delta_i_deg;
    man->delta_VzII = (man->delta_i_rad * sin(man->phi1z) / sin(man->phi1z - man->phi2z))
                      * man->delta_gamma_deg / man->delta_i_deg;

    man->delta_Vt_I1 = man->delta_VtI * man->delta_aI / man->delta_aS;
    man->delta_Vt_I2 = man->delta_VtII * man->delta_aI / man->delta_aS;
    man->delta_Vt_II1 = man->delta_VtI * man->delta_aII / man->delta_aS;
    man->delta_Vt_II2 = man->delta_VtII * man->delta_aII / man->delta_aS;

    man->delta_Vz_I1 = man->delta_VzI * man->delta_aI / man->delta_aS;
    man->delta_Vz_I2 = man->delta_VzII * man->delta_aI / man->delta_aS;
    man->delta_Vz_II1 = man->delta_VzI * man->delta_aII / man->delta_aS;
    man->delta_Vz_II2 = man->delta_VzII * man->delta_aII / man->delta_aS;

    // Сдвиг тормозных импульсов на пол-витка (в пределах текущего витка)
    shift_braking_angle(&man->delta_aI, &man->phi1);
    shift_braking_angle(&man->delta_aI, &man->phi2);
    shift_braking_angle(&man->delta_aII, &man->phi3);
    shift_braking_angle(&man->delta_aII, &man->phi4);

    // Если внезапно оказалось, что торможение начинается раньше разгона, меняем порядок действий
    if (man->phi2 < man->phi1)
    {
        swap(&man->phi2, &man->phi1);
        swap(&man->delta_Vt_I2, &man->delta_Vt_I1);
        swap(&man->delta_Vz_I2, &man->delta_Vz_I1);
    }
    if (man->phi4 < man->phi3) {
        swap(&man->phi4, &man->phi3);
        swap(&man->delta_Vt_II2, &man->delta_Vt_II1);
        swap(&man->delta_Vz_II2, &man->delta_Vz_II1);
    }


    man->delta_tr = man->delta_Vt_I1 * K(man->phi1)
                    + man->delta_Vt_I2 * K(man->phi2)
                    + man->delta_Vt_II1 * K(man->phi3)
                    + man->delta_Vt_II2 * K(man->phi4);

    man->time_error = man->delta_t_initial - man->delta_tr;
    man->next_delta_t = man->delta_t + man->time_error;

    return man;
}

void shift_braking_angle(double *a, double *phi) {
    if (*a < 0.0) {
        if (sin(*phi) > 0)
            *phi += M_PI;
        else
            *phi -= M_PI;
    }
}

void swap(double *a, double *b) {
    double c = *a;
    *a = *b;
    *b = c;
}

double K(double phi) {
    return -3 * phi + 4 * sin(phi);
}

void calculate_initial(struct INPUT_DATA *input, struct MANEUVERS_DATA *man) {
    // Большая полуось
    man->a_H = Re + (input->start.H_max + input->start.H_min) / 2;
    man->a_k = Re + (input->rendezvous.H_max + input->rendezvous.H_min) / 2;
    // Эксцентриситет
    man->e_H = (input->start.H_max - input->start.H_min) / (2 * man->a_H);
    man->e_k = (input->rendezvous.H_max - input->rendezvous.H_min) / (2 * man->a_k);
    // Опорная орбита
    man->r0 = (man->a_H + man->a_k) / 2;
    man->V0 = sqrt(mju / man->r0);
    man->lambda = man->V0 / man->r0;


    // Отклонение большой полуоси
    man->delta_aS_initial = man->a_k - man->a_H;
    man->delta_a_initial = man->delta_aS_initial / man->r0;

    man->delta_e_x = man->e_k * cos(input->rendezvous.U_prg) - man->e_H * cos(input->start.U_prg);
    man->delta_e_y = man->e_k * sin(input->rendezvous.U_prg) - man->e_H * sin(input->start.U_prg);
    man->delta_e = sqrt(pow(man->delta_e_x, 2) + pow(man->delta_e_y, 2));

    int N1 = input->start.N0;
    man->phi_e = atan(man->delta_e_y / man->delta_e_x);

    if (man->delta_e_x < 0 && man->delta_e_y < 0)
        man->phi_e += M_PI;

    man->phi1_initial = man->phi_e + 2 * M_PI * (N1 - input->start.N_ap) - input->U_ap;
    man->k1 = 4 * sin(man->phi1_initial) - 3 * man->phi1_initial;

    man->T_H = 2 * M_PI * man->a_H * sqrt(man->a_H / mju);
    man->T_k = 2 * M_PI * man->a_k * sqrt(man->a_k / mju);
    man->t_H = man->T_H * (input->start.N_ap - input->start.N0 - 1 + (2 * M_PI - input->start.U) / (2 * M_PI));
    man->t_k = man->T_k *
               (input->rendezvous.N_ap - input->rendezvous.N0 - 1 + (2 * M_PI - input->rendezvous.U) / (2 * M_PI));

    man->delta_SIGMA = input->rendezvous.SIGMA - input->start.SIGMA;
    man->delta_i_rad = input->rendezvous.i - input->start.i;
    man->delta_i_deg = man->delta_i_rad * RAD_TO_DEG;

    man->next_delta_t = man->lambda * (man->t_k - man->t_H);
    man->delta_t_initial = man->next_delta_t;
    man->u_z = atan(sin(input->start.i) * man->delta_SIGMA / man->delta_i_rad);
    man->delta_gamma_rad = man->delta_SIGMA * sin(input->start.i) / sin(man->u_z);
    man->delta_gamma_deg = man->delta_gamma_rad * RAD_TO_DEG;
    // Выбираем ближайший к phi_e угол
    man->phi_z = man->u_z + M_PI * floor((man->phi_e - man->u_z) / M_PI);
    man->delta_phi = man->phi_e - man->phi_z;
}
